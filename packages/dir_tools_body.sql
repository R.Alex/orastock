CREATE OR REPLACE PACKAGE BODY Dir_Tools
IS

  --===================================================
  FUNCTION Is_Directory_Object_Exists(p_Dir IN VARCHAR2) RETURN BOOLEAN
  IS
    cnt  NUMBER;
  BEGIN
    SELECT count(*) INTO cnt FROM all_directories
      WHERE upper(directory_name) = upper(p_Dir);
    IF cnt = 0 THEN
      RETURN FALSE;
    END IF;
    RETURN TRUE;
  END Is_Directory_Object_Exists;


  --======================================================
  FUNCTION File_Exists (Directory_Name    IN VARCHAR2,
                        File_Name         IN VARCHAR2)
  RETURN BOOLEAN
  IS
    fexists BOOLEAN := FALSE;
    file_len NUMBER := 0;
    block_size NUMBER := 0;
  BEGIN
    IF Directory_Name IS NULL OR File_Name IS NULL THEN
      RETURN FALSE;
    END IF;

    utl_file.fgetattr(Directory_Name, File_Name,
                      fexists, file_len, block_size);
    RETURN fexists;
  END File_Exists;


END Dir_Tools;
/
