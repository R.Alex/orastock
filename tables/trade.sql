CREATE SEQUENCE seq_trade;
/

CREATE TABLE trade
(
  trade_id        number primary key,
  portfolio_id    number       not null,
  emit_code       varchar2(10) not null, -- �����
  trade_date      date         not null,
  operation       char(1)      not null, -- B/S (Buy/Sell)
  qty             number       not null, -- ���������� �����
  price           number                 -- ���� 1 �����
);

ALTER TABLE trade ADD CONSTRAINT fk_trade_portfolio
  FOREIGN KEY(portfolio_id) REFERENCES portfolio(portfolio_id);

CREATE OR REPLACE TRIGGER trg_trade_bir 
  BEFORE INSERT ON trade FOR EACH ROW
BEGIN
    SELECT seq_trade.nextval INTO :new.trade_id FROM dual;
END;
/
