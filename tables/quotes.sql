CREATE TABLE quotes -- ������� ���������
(
   emit_id     int not null,
   emit_code   varchar2(10) not null,
   dt_date     date not null,
   dt_year     as (extract(year from dt_date)),
   dt_quarter  as (to_number(to_char(dt_date, 'Q'))),
   dt_month    as (extract(month from dt_date)),
   dt_day      as (extract(day from dt_date)),
   o           number,  -- open
   c           number,  -- close
   h           number,  -- high
   l           number,  -- low
   vol         number   -- volume
)
PARTITION BY RANGE (emit_id)
INTERVAL (1)
(PARTITION PARTISN_01 VALUES LESS THAN (2))
ENABLE ROW MOVEMENT;

ALTER TABLE quotes ADD CONSTRAINT fk_quotes_emit FOREIGN KEY(emit_id)
  REFERENCES emit(emit_id);
