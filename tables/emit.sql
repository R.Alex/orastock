CREATE SEQUENCE seq_emit;
/

CREATE TABLE emit
(
    emit_id          int not null,
    emit_code        varchar2(10) not null, -- �����
    emit_name        varchar2(100) not null, -- �������� ��������
    sector           varchar2(100), -- ������
    industry_group   varchar2(100), -- ���������� ������
    lot_size         smallint default 1, -- ������ ����
    CONSTRAINT pk_emit_code PRIMARY KEY(emit_id),
    CONSTRAINT uk_emit_code UNIQUE(emit_code)
);
/

CREATE OR REPLACE TRIGGER trg_emit_bir 
  BEFORE INSERT ON emit FOR EACH ROW
BEGIN
    SELECT seq_emit.nextval INTO :new.emit_id FROM dual;
END;
/

INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('Fix Price Group', 'FIXP', '��������������� ������', '��������� ��������� ��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('GTL', 'GTLC', '��������������', '������������� � ��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('Global Ports Investments', 'G8P', '��������������', '������ ���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('O''Key Group', 'OKEY', '������ ������������� ������', '�������� �������� ������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('OR Group (����� ������)', 'ORUP', '��������������� ������', '��������, ������, �������� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('Ozon Holdings', 'OZON', '��������������� ������', '����������� ���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('QIWI', 'QIWI', '�������', '��������� ���������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('Raven Property group', 'RAVN', '������������', '���������� � �������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('TCS Group Holdings', 'TCSG', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('United Medical Group', 'GEMC', '���������������', '����������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('VK', 'MAIL', '�������������� ����������', '��-������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('X5 Group', 'FIVE', '������ ������������� ������', '�������� �������� ������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ��������', 'AVAN', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ��������', 'PRMB', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������ ����������� ��������', 'ALRS', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ��������', 'BANE', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� ��', 'ARSA', '�������', '����� ��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� �������', 'AFKS', '�������', '��������� ���������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����� � �����', 'ABRD', '������ ������������� ������', '�������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ �����', 'UTAR', '��������������', '�������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ���� ���', 'SIBG', '������ ������������� ������', '�������� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����', 'AKRN', '���������', '��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� ���� 36,6', 'APTK', '������ ������������� ������', '�������� �������� ������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����-�����������', 'ACKO', '�������', '�����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ �������������� ��������', 'ASSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� ���������������� �����', 'AMEZ', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� � ���������� ���������', 'AFLT', '��������������', '�������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���� �������', 'USBN', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���� ���', 'VTBR', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���� ���������', 'KUZB', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���� �����-���������', 'BSPB', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������', 'BISV', '���������������� ������', '��������� �������������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����', 'BLNG', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������ �����', 'BELU', '������ ������������� ������', '�������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����������', 'BRZL', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����-������ ����������', 'VSMO', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ���������� �����', 'VLHZ', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������������', 'VGSB', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� ���������������� �����', 'VSYD', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�� ��������', 'SMLT', '������������', '������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ���������� ������', 'GMKN', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'GAZP', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������� ����������������� ������-��-����', 'RTGZ', '������������ ������', '��������� ������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������� �����', 'SIBN', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'GTRK', '��������������', '������������� � �� ���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������� ������������� ����������', 'GRNT', '������������ ������', '��������� ������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������ ���', 'HMSG', '����������', '�������������� ������������ � ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������ ���', 'LSRG', '������������', '���������� � �������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������ ���������', 'GCHE', '������ ������������� ������', '�������� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ �������������� ��������', 'DASB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������� �������������� ��������', 'DVEC', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������� ������� �����������', 'FESH', '��������������', '������ ���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������� ���', 'DSKY', '������ ������������� ������', '�������� �������� ������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������� ����� ������������', 'DZRDP', '�������������� ����������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� ��������������', 'EELT', '��������������', '������������� ������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������', 'ZVEZ', '��������������� ������', '������ ��� �����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����� ����� �.�. ��������', 'ZILL', '��������������� ������', '����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����� ������������� ������� � ����������...', 'DIOD', '���������������', '�������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�� ����-������', 'RUSI', '�������', '����� ��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������-�����������', 'IDVP', '������������', '���������� � �������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������', 'INGR', '������������', '���������� � �������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'IGST', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� ��������� ������ ��������', 'ISKJ', '���������������', '���������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����� ���', 'IRAO', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����� ���', 'IRKT', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������', 'IRGZ', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����', 'KMAZ', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������������', 'KZOS', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������� �������� ��������', 'KLSB', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������', 'KCHE', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������ ������������ ��������', 'TGKD', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� ������������ �����', 'KMEZ', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ���', 'KOGK', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� �������� ��������', 'KTSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������������', 'KRSBP', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������', 'KAZT', '���������', '��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� ������������ ��������', 'KGKC', '������������ ������', '��������� ������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������ ��', 'LKOH', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������', 'LVHK', '�������������� ����������', '����������� ������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������', 'LNZL', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����', 'LNTA', '������ ������������� ������', '������ ������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� �������������� ��������', 'LPSB', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�.�����', 'MVID', '��������������� ������', '��������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������', 'MERF', '�������', '�������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����', 'GEMA', '���������������', '���������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'MSTT', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���� ������-������', 'MRKZ', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���� �����', 'MRKU', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������', 'MAGE', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������', 'MGNT', '������ ������������� ������', '������ ������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������� ���������������� ��������', 'MAGN', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������', 'ODVA', '���������������� ������', '���������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����', 'MTLR', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������� �����������', 'MTSS', '���������������� ������', '��������� �������������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� �������������� ��������', 'MRSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� �����', 'MOEX', '�������', '����� ��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� ��������� ���������� ����', 'MGTS', '���������������� ������', '��������� �������������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� ������������ ������� ������� �������', 'KROT', '������ ������������� ������', '�������� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� ��������� ����', 'CBOM', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������', 'MSNG', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������', 'MSST', '��������������� ������', '��������� ��������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���-����������������', 'VJGZ', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'NVTK', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� �����', 'NAUK', '��������������', '��������������� � ��������� ��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����-�����', 'NSVZ', '���������������� ������', '��������� �������������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ���������', 'NFAZ', '��������������� ������', '����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������������', 'NKNC', '���������', '��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������', 'NKSH', '��������������� ������', '������ ��� �����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ���������������� ��������', 'NLMK', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������� �������� ��������������', 'NKHP', '������ ������������� ������', '������ ������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������� ������� �������� ����', 'NMTP', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���-2', 'OGKB', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ �������� �������� ���', 'UWGN', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ���������������� ����������', 'UNAC', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ��������� �������', 'UCSS', '�������', '�������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ������������������ ������', 'OMZZ', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���-������������������ ����������', 'PIKK', '������������', '������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� �������������� ��������', 'PMSB', '������������ ������', '������������ ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� ����������', 'POLY', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����', 'PMSBP', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���', 'RBCM', '���������������� ������', '���������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ������� ����� �.�. �������', 'RKKE', '��������������', '��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��-�������� ������', 'CHGZ', '������������', '������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'ROSB', '�������', '�������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� ���������� �������', 'ROST', '��������������� ������', '��������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����', 'RUAL', '���������', '���������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'RASP', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������� �����������', 'RENI', '�������', '�����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'RLMN', '���������', '��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'RDRB', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����������', 'RGSS', '�������', '�����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� ��', 'ROSN', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'MRKH', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'RTKM', '���������������� ������', '��������� �������������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������', 'HYDR', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'AGRO', '������ ������������� ������', '�������� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� �������', 'RUGR', '������ ������������� ������', '�������� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������', 'ROLO', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������� ��', 'RNFT', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������� ������������', 'AQUA', '��������������� ������', '������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������� �������������� �������� ������...', 'RZSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������', 'SAGO', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� ���', 'KRKN', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������', 'SARE', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������������', 'SLEN', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������', 'SBER', '�������', '�����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'CHMF', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������', 'SELG', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������-��������������', 'MFGS', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������-���������������������', 'JNOS', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� ��������� �����', 'MGNZ', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������� ����', 'SVAV', '��������������� ������', '����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������', 'SFDGK', '�������������� ����������', '�������������� ����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������������', 'STSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������', 'SNGS', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ������', 'TNSE', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ������ �������', 'VRSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ������ ����� ��', 'MISB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ������ ������ ��������', 'NNSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ������ ������-��-����', 'RTSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ������ ���������', 'YRSB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ����������������� ����� ���...', 'KRKO', '��������������', '������������� ������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���������� �������������� ��������', 'TASB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� ����� �.�. ������', 'TATN', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'TTLK', '���������������� ������', '��������� �������������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������� ������������ �������� �1', 'TGKA', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������������� ������������ �������� �2', 'TGKB', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������� ����������������� ��������', 'TORS', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'TRNF', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������� ���������������� ��������', 'TRMK', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������ ����� ���������������', 'TUZA', '��������������', '������� � ������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� �������� ����� �������', 'UKUZ', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��������� �������', 'URKZ', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��� ���', 'FEES', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������', 'LIFE', '���������������', '������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'PHOR', '���������', '��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������', 'HIMC', '���������', '��������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����', 'CIAN', '������������', '���������� � �������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����� ������������� ��������', 'WTCM', '������������', '���������� � �������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� ��������', 'CNTL', '���������������� ������', '��������� �������������������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� ����� ���������������� �����...', 'PRFN', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� ��������-��������� �����', 'CHKZ', '��������������� ������', '������ ��� �����������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� ���������������� ��������', 'CHMK', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����������� �������������� �����', 'CHEP', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('��+ ����', 'ENGP', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�����������', 'ELTZ', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('���� ������', 'ENRU', '������������ ������', '������������� ��������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������', 'SFIN', '�������', '��������� ���������� ������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������', 'ETLN', '������������', '���������� � �������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('����-��������� ��������� ��������', 'UNKL', '���������', '������� � �����', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������', 'UPRO', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('�������� ��������-�������������� �������...', 'YAKG', '����������', '�����, ���, ������ ���� �������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������������', 'YKEN', '������������ ������', '����������������', 1);
INSERT INTO emit(emit_name, emit_code, sector, industry_group, lot_size) VALUES
('������', 'YNDX', '�������������� ����������', '��-������', 1);
