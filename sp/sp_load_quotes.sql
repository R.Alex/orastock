CREATE OR REPLACE PROCEDURE sp_load_quotes
(
  p_directory    IN varchar2 := 'raw_dir',
  p_file         IN varchar2,
  p_emit_code    IN varchar2,
  p_rows_loaded  OUT number
)
AS
    qry               VARCHAR2(8000);
    cnt               INT;
BEGIN
    p_rows_loaded := 0;
    -- �������� ������� ������� ���������� � �����
    IF NOT Dir_Tools.Is_Directory_Object_Exists(p_directory) THEN
        RAISE std.dir_not_found;
    END IF;

    sp_log('222');
    IF NOT Dir_Tools.File_Exists(p_directory, p_file) THEN
        RAISE std.file_not_found;
    END IF;

    sp_log('333');
    -- ������� ������� ������� ����� ���������
    SELECT count(1) INTO cnt FROM user_tables WHERE TABLE_NAME = 'RAW_QUOTES';
    IF cnt > 0 THEN
      EXECUTE IMMEDIATE 'DROP TABLE RAW_QUOTES';
    END IF;

    sp_log('444');
    -- ������� ������� �������
    qry := 'CREATE TABLE raw_quotes'||
      '('||
        'dt_date varchar2(20),'||
        'dt_time varchar2(20),'||
        'o       varchar2(20),'||
        'h       varchar2(20),'||
        'l       varchar2(20),'||
        'c       varchar2(20),'||
        'vol     varchar2(20)'||
      ')'||
      ' organization external ('||
      '  type oracle_loader'||
      '  default directory '|| p_directory ||
      '  access parameters ('||
      '   records delimited  by newline'||
      '   badfile '||p_directory||':'''||p_file||'.bad'''||
      '   fields terminated by '';'' '||
      '   missing field values are null'||
      '   skip 1'||
      '  )'||
      '  location ('''||p_file||''')'||
      ' ) reject limit unlimited';

    sp_log(qry);
    EXECUTE IMMEDIATE qry;

    qry :=
      'MERGE INTO quotes q USING raw_quotes r'||
      ' ON r.dt_date = to_char(q.dt_date, ''DD-MM-YYYY'') AND r.emit_code = p_emit_code'||
      ' WHEN MATCHED THEN'||
      '   UPDATE SET q.o = to_number(r.o), q.h = to_number(r.h),'||
      '              q.l = to_number(r.l), q.c = to_number(r.c),'||
      '              q.vol = to_number(r.vol)'||
      ' WHEN NOT MATCHED THEN'||
      '   INSERT (q.emit_code, q.dt_date, q.o, q.c, q.h, q.l)'||
      '   VALUES (p_emit_code, to_date(r.dt_date, ''DD-MM-YYYY''),'||
      '     to_number(r.o), to_number(r.h), to_number(r.l), to_number(r.c), to_number(r.vol));';
      
    --EXECUTE IMMEDIATE qry;
    cnt := SQL%ROWCOUNT;
    
EXCEPTION
  WHEN std.dir_not_found THEN
      sp_log('Directory object '||nvl(p_directory, '<null>')||' not found.', 'E');
  WHEN std.file_not_found THEN
      sp_log('File not found: '||nvl(p_file, '<null>'), 'E');
END;
/