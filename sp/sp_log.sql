CREATE OR REPLACE PROCEDURE sp_log
(
  p_log_msg  IN varchar2,
  p_log_type IN char := 'I'
)
IS
	PRAGMA AUTONOMOUS_TRANSACTION;
	l_start 	int := 1;  			-- ��������� ������� ���������� ���������
	l_msg 		varchar(255);       -- ���������� ���������
BEGIN
	l_msg := substr(p_log_msg, 1, 255);
	WHILE l_msg IS NOT NULL LOOP
	    INSERT INTO audit_log(log_type, log_msg) VALUES(p_log_type, l_msg);
	    l_msg := '';
	    l_start := l_start + 255;
	    IF length(p_log_msg) >= l_start THEN
	    	l_msg := substr(p_log_msg, l_start, 255);
	    END IF;
	END LOOP;

    COMMIT;
END sp_log;
/

