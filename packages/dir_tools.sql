CREATE OR REPLACE PACKAGE Dir_Tools
IS

  FUNCTION Is_Directory_Object_Exists(p_Dir IN VARCHAR2) RETURN BOOLEAN;
   
  FUNCTION File_Exists (Directory_Name    IN VARCHAR2,
                        File_Name         IN VARCHAR2)
          RETURN BOOLEAN;

END Dir_Tools;
/
