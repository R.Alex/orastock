CREATE OR REPLACE PACKAGE Std -- ����������� ��������� � ���� ������ ��� STOCK
IS
    -- ����������
    dir_not_found EXCEPTION; -- ������ ���������� �� ����������
    PRAGMA EXCEPTION_INIT(dir_not_found, -20006);
    file_not_found EXCEPTION; -- ���� �� ������
    PRAGMA EXCEPTION_INIT(file_not_found, -20007);
END Std;
/
