CREATE TABLE portfolio
(
  portfolio_id    int primary key,
  parent_id       int,
  created_dt      date,
  portfolio_name  varchar2(100) not null,
  portfolio_type  varchar2(30)
);

ALTER TABLE portfolio ADD CONSTRAINT fk_portfolio 
  FOREIGN KEY(parent_id)
  REFERENCES portfolio(portfolio_id);

