CREATE SEQUENCE seq_audit_log;
/

CREATE TABLE audit_log
(
     log_id        number,
     log_type      char(1) default 'I', --I/W/E Info/Warning/Error
     log_msg       varchar2(255),
     log_time      timestamp default systimestamp
)
/

CREATE OR REPLACE TRIGGER trg_audit_log_bir 
  BEFORE INSERT ON audit_log FOR EACH ROW
BEGIN
    SELECT seq_audit_log.nextval INTO :new.log_id FROM dual;
END;
/

